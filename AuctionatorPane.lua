local AceOO = AceLibrary("AceOO-2.0")
local zc = gAtrZC;

AtrPane = AceOO.Class()

ATR_SHOW_CURRENT	= 1;
ATR_SHOW_HISTORY	= 2;
ATR_SHOW_HINTS		= 3;

function AtrPane.prototype:init(which)
    AtrPane.super.prototype.init(self) -- very important. Will fail without this.

		self.fullStackSize = 0
		self.id = which
		self.width = 0--(MIN_TOOLTIP_SIZE - 20)*tablet.fontSizePercent
		self.tablet = tablet
		self.title = "Title"
    self.fullStackSize	= 0;
    self.totalItems		= 0;		-- total in bags for this item

    self.UINeedsUpdate	= false;
    self.showWhich		= ATR_SHOW_CURRENT;
	
    self.activeScan = nil;
    self.activeSearch	= nil;
    self.sortedHist		= nil;
    self.hints			= nil;
	
    self.hlistScrollOffset	= 0;
    self:ClearSearch();
end

-----------------------------------------

function AtrPane.prototype:DoSearch (searchText, exact, rescanThreshold, callback)
--if self == nil then self = this end
	self.currIndex			= nil;
	self.histIndex			= nil;
	self.hintsIndex			= nil;
	
	self.sortedHist			= nil;
	self.hints				= nil;
	
	self.SS_hilite_itemName	= searchText;		-- by name for search summary
	
	Atr_ClearBuyState();

	self.activeScan = Atr_FindScan (nil);
	
	Atr_ClearAll();		-- it's fast, might as well just do it now for cleaner UE
	
	self.UINeedsUpdate = false;		-- will be set when scan finishes
	self.activeSearch = AtrSearch:new(searchText, exact, rescanThreshold, callback);

	if (exact) then
		self.activeScan = self.activeSearch:GetFirstScan();
	end
	
	local cacheHit = false;

	if (searchText ~= "") then
		if (self.activeScan.whenScanned == 0) then		-- check whenScanned so we don't rescan cache hits
			self.activeSearch:Start();
		else
			self.UINeedsUpdate = true;
			cacheHit = true;
		end
	end

	return cacheHit;
end
-----------------------------------------

function AtrPane.prototype:ClearSearch ()
	self:DoSearch ("", true);
end

-----------------------------------------

function AtrPane.prototype:GetProcessingState ()
	
	if (self.activeSearch) then
		return self.activeSearch.processing_state;
	end
	
	return KM_NULL_STATE;
end
-----------------------------------------

function AtrPane.prototype:IsScanEmpty ()
	
	return (self.activeScan == nil or self.activeScan:IsNil());
	
end
-----------------------------------------

function AtrPane.prototype:ShowCurrent ()
	
	return self.showWhich == ATR_SHOW_CURRENT;
	
end
-----------------------------------------

function AtrPane.prototype:ShowHistory ()
	
	return self.showWhich == ATR_SHOW_HISTORY;
	
end
-----------------------------------------

function AtrPane.prototype:ShowHints ()
	
	return self.showWhich == ATR_SHOW_HINTS;
	
end

-----------------------------------------

function AtrPane.prototype:SetToShowCurrent ()
	
	self.showWhich = ATR_SHOW_CURRENT;
	
end
-----------------------------------------

function AtrPane.prototype:SetToShowHistory ()
	
	self.showWhich = ATR_SHOW_HISTORY;
	
end
-----------------------------------------

function AtrPane.prototype:SetToShowHints ()
	
	self.showWhich = ATR_SHOW_HINTS;
	
end

