local zc = gAtrZC;
---------------------------------------------
local ATR_SELL_NULL					  = 0;
local ATR_SELL_READY		  		= 1;
local ATR_SELL_COMBINE  			= 2;
local ATR_SELL_SPLIT          = 3;
local ATR_SELL_POST	          = 4;

local gSellState = ATR_SELL_NULL;
local gAtr_Sell_CurRequest = nil;
local ProcessingRequest = false;
--------------------------------------------
--FUNCTIONS
local Original_PickupContainerItem = nil;
local Original_SplitContainerItem = nil;

local clearAuctionItem;

--------------------------------------------
-- Search and return same item and stacksize or fist find item with same name
--------------------------------------------
function Atr_Sell_StartAuction(itemName, stackStartingPrice, stackBuyoutPrice, duration, StackSize, NumStacks)
  
  local auctionName, _, auctionCount = GetAuctionSellItemInfo();
  local auctionLink = GetContainerItemLink(findAuctionItem());
  local bag, slot = findAuctionItem();

  clearAuctionItem();

  local request = {};
  request.name = itemName;
  request.itemLink = auctionLink;
  request.stackSize = StackSize;
  request.stackCount = NumStacks;
  request.bid = stackStartingPrice;
  request.buyout = stackBuyoutPrice;
  request.duration = duration;
  request.stackPostCount = 0; 

  gSellState = ATR_SELL_READY;
  gAtr_Sell_CurRequest = request;
end;
------------------------------

function Atr_Sell_FinishAuction()
  zc.md("FINISHED");
  
  clearAuctionItem();
  gSellState = ATR_SELL_NULL;
  gAtr_Sell_CurRequest = nil;
  Atr_Sell_EndProcessingRequest();
end;
------------------------------------

function Atr_Sell_Processing(request)
  
  if (not request) then return false; end;
 
  local stack1 = Atr_Sell_StackByLinkSize(request.itemLink, request.stackSize)

  if (request.stackCount == request.stackPostCount ) then
    Atr_Sell_FinishAuction();
    return
  end
  
  if ( not stack1 ) then zc.md("stack1 nil state="..tostring(gSellState)); return; end
  
  local _, stack1Size, locked = GetContainerItemInfo(stack1.bag, stack1.slot);

  if (locked or CursorHasItem() or GetAuctionSellItemInfo()) then 
    zc.md("locked or CursorHasItem or GetAuctionSellItemInfo");
    return; 
    end

  if (stack1Size == request.stackSize ) then
  
    if ( gSellState == ATR_SELL_POST) then
       zc.md("wait auction posted");
      return;
    end;

    gSellState = ATR_SELL_POST;
    clearAuctionItem();

    pickupContainerItem(stack1.bag, stack1.slot);
    ClickAuctionSellItemButton();
    ClearCursor();
    AuctionsFrameAuctions_ValidateAuction();

    local name, texture, count, quality, canUse, price = GetAuctionSellItemInfo();
    if (request.bid and request.buyout and request.duration and name) then
      zc.md("StartAuction posted");
      -- these 3 lines help with compatability
      MoneyInputFrame_SetCopper(StartPrice, request.bid);
      MoneyInputFrame_SetCopper(BuyoutPrice, request.buyout);
      AuctionFrameAuctions.duration = request.duration;

      StartAuction(request.bid, request.buyout, durationMinutes(request.duration));
      request.stackPostCount = request.stackPostCount + 1;
    else
      zc.msg("Fail "..tostring(name).." "..tostring(stack1.bag).." "..tostring(stack1.slot).." with "..tostring(request.bid).." "..tostring(request.buyout).." "..tostring(request.duration))
      Atr_Sell_FinishAuction();
    end
   
  elseif (gSellState == ATR_SELL_COMBINE or gSellState == ATR_SELL_SPLIT) then
    zc.md("state "..tostring(gSellState).." WAITING")
  elseif (stack1Size < request.stackSize) then
    --combine
    local stack2 = Atr_Sell_StackByLinkSize(request.itemLink, nil, stack1.bag, stack1.slot + 1);
    if (stack2) then
      local _, stack2Size = GetContainerItemInfo(stack2.bag, stack2.slot);
      if (stack1Size + stack2Size <= request.stackSize) then
        zc.md("Combine "..tostring(stack1.bag)..tostring(stack1.slot).." with "..tostring(stack2.bag)..tostring(stack2.slot))
        -- Combine all of stack2 with stack1.
        --setState(request, COMBINING_STACK_STATE);
        gSellState = ATR_SELL_COMBINE;
        pickupContainerItem(stack2.bag, stack2.slot);
        pickupContainerItem(stack1.bag, stack1.slot);
        --request.stack = stack1;
      else
        zc.md("Combine with Split "..tostring(stack1.bag)..tostring(stack1.slot).." with "..tostring(stack2.bag)..tostring(stack2.slot))
        -- Combine part of stack2 with stack1.
        --setState(request, SPLITTING_AND_COMBINING_STACK_STATE);
        gSellState = ATR_SELL_COMBINE;
        splitContainerItem(stack2.bag, stack2.slot, request.stackSize - stack1Size);
        pickupContainerItem(stack1.bag, stack1.slot);
      end
      --Atr_Sell_StartAuction(stack1.bag, stack1.slot, 100, 1000 ,1)
      pickupContainerItem(stack1.bag, stack1.slot);
      ClickAuctionSellItemButton();
      ClearCursor();
    else
      -- Not enough of the item found!
      zc.msg('FrmtNoEmptyPackSpace');
      Atr_Sell_FinishAuction();
    end
  else
    -- The stack we have is more than needed. Locate an empty slot.
    local stack2 = findEmptySlot();
    if (stack2) then
      --setState(request, SPLITTING_STACK_STATE);
      gSellState = ATR_SELL_SPLIT;
      zc.md("Split "..tostring(stack1.bag).." "..tostring(stack1.slot).." "..tostring(stack2.bag).." "..tostring(stack2.slot).." "..tostring(request.stackSize))
  
      splitContainerItem(stack1.bag, stack1.slot, request.stackSize);
      pickupContainerItem(stack2.bag, stack2.slot);
          
      --Atr_Sell_StartAuction(stack2.bag, stack2.slot, 100, 1000 ,1)
      pickupContainerItem(stack2.bag, stack2.slot);
      ClickAuctionSellItemButton();
      ClearCursor();
          
    else
      -- Not enough of the item!
      local output = string.format('FrmtNotEnoughOfItem', request.name);
      zc.msg(output);
      Atr_Sell_FinishAuction();
    end
  end
end
-----------------------


function Atr_Sell_StackByLinkSize(itemLink, stackSize, startingBag, startingSlot)
  local sameItem = nil;
  if (stackSize == nil) then stackSize = 0;	end
	if (startingBag == nil) then startingBag = 0;	end
	if (startingSlot == nil) then	startingSlot = 1;	end
  
	for bag = startingBag, 4, 1 do
		if (GetBagName(bag)) then
			local numItems = GetContainerNumSlots(bag);
			if (startingSlot <= numItems) then
				for slot = startingSlot, GetContainerNumSlots(bag), 1 do
          
  				local thisItemLink = GetContainerItemLink(bag, slot);
          local _, thisStackSize = GetContainerItemInfo(bag, slot);

					if (itemLink == thisItemLink) then 
            if ( stackSize == 0 or stackSize == thisStackSize) then return { bag=bag, slot=slot }; end
            if ( not sameItem ) then sameItem = { bag=bag, slot=slot }; end
          end
				end
			end
			startingSlot = 1;
		end
	end
	return sameItem;
end 
-----------------------------------------------------------------------------------------------------------------------

function Atr_Sell_Idle ()
if (Atr_Sell_BeginProcessingRequest()) then
		Atr_Sell_Processing(gAtr_Sell_CurRequest);
  end
end
--------------------------------------------------

function Atr_Sell_Cancel (msg)
	gSellState = ATR_SELL_NULL;
end
-------------------------------------------

function Atr_Sell_OnAuctionUpdate()
	return (gSellState ~= ATR_SELL_NULL);
end

function Atr_Sell_OnAuctionOwnedUpdate()
	if (gSellState == ATR_SELL_POST) then
    gSellState = ATR_SELL_READY;
    end
end
--------------------------------------------------

function Atr_Sell_BeginProcessingRequest()
	if (not ProcessingRequest and
		AuctionFrame and AuctionFrame:IsVisible() and
		gAtr_Sell_CurRequest) then

		ProcessingRequest = true;
		zc.md("Begin processing the post queue");

		-- Hook the functions to disable picking up items. This prevents
		-- spurious ITEM_LOCK_CHANGED events from confusing us.
		if (not Original_PickupContainerItem) then
			Original_PickupContainerItem = PickupContainerItem;
			PickupContainerItem = AucPostManager_PickupContainerItem;
		end
		if (not Original_SplitContainerItem) then
			Original_SplitContainerItem = SplitContainerItem;
			SplitContainerItem = AucPostManager_SplitContainerItem;
		end
	end
	return ProcessingRequest;
end

-------------------------------------------------------------------------------
-- Ends processing the request queue
-------------------------------------------------------------------------------
function Atr_Sell_EndProcessingRequest()
  zc.md("Unhook the functions "..tostring(ProcessingReques));
	if (ProcessingRequest) then
		-- Unhook the functions.
    zc.md("Unhook the functions");
		if (Original_PickupContainerItem) then
			PickupContainerItem = Original_PickupContainerItem;
			Original_PickupContainerItem = nil;
		end
		if (Original_SplitContainerItem) then
			SplitContainerItem = Original_SplitContainerItem;
			Original_SplitContainerItem = nil;
		end

		zc.md("End processing the post queue");
		ProcessingRequest = false;
	end
end
------------------------------------------------------------

-------------------------------------------------------------------------------
-- Wrapper around PickupContainerItem() that always calls the Blizzard version.
-------------------------------------------------------------------------------
function pickupContainerItem(bag, slot)
	if (Original_PickupContainerItem) then
		return Original_PickupContainerItem(bag, slot);
	end
	return PickupContainerItem(bag, slot);
end

-------------------------------------------------------------------------------
-- Wrapper around SplitContainerItem() that always calls the Blizzard version.
-------------------------------------------------------------------------------
function splitContainerItem(bag, slot, count)
	if (Original_SplitContainerItem) then
		return Original_SplitContainerItem(bag, slot, count);
	end
	return SplitContainerItem(bag, slot, count);
end 
-------------------------------------------------------------------------------

function clearAuctionItem()
	local bag, item = findAuctionItem();
   -- zc.md(tostring(bag).." "..tostring(item))
	if (bag and item) then
		ClickAuctionSellItemButton();
		pickupContainerItem(bag, item);
	end
end
-----------------------------------------------------------

function findEmptySlot()
	for bag = 0, 4, 1 do
		if (GetBagName(bag)) then
			for item = GetContainerNumSlots(bag), 1, -1 do
				if (not GetContainerItemInfo(bag, item)) then
					return { bag=bag, slot=item };
				end
			end
		end
	end
	return nil;
end
-----------------------------------

function getContainerItemName(bag, slot)
	local link = GetContainerItemLink(bag, slot);
	if (link) then
		local _, _, _, _, name = breakLink(link);
		return name;
	end
end
------------------------------------

-- Given a Blizzard item link, breaks it into it's itemID, randomProperty, enchantProperty, uniqueness and name
function breakLink(link)
	if (type(link) ~= 'string') then return end
	local i,j, itemID, enchant, randomProp, uniqID, name = string.find(link, "|Hitem:(%d+):(%d+):(%d+):(%d+)|h[[]([^]]+)[]]|h")
	return tonumber(itemID or 0), tonumber(randomProp or 0), tonumber(enchant or 0), tonumber(uniqID or 0), name
end
------------------------------------

function durationHours(duration)
  if ( duration == 1 ) then  return 2; end;
  if ( duration == 2 ) then  return 8; end;
  if ( duration == 3 ) then  return 24; end;
return 0;
end;

function durationMinutes(duration)
  if ( duration == 1 ) then  return 120; end;
  if ( duration == 2 ) then  return 480; end;
  if ( duration == 3 ) then  return 1440; end;
return 0;
end;