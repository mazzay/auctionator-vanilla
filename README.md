# README #

Attempt backport wow addon to Vanilla.

Original Vanilla 1.0.0 version with fixed layout.
[Auctionator 1.0.0](https://bitbucket.org/mazzay/auctionator-vanilla/downloads/Auctionator_1.0.0.zip)

Backported WotLK 2.6.3 version.
[Auctionator 2.6.3](https://bitbucket.org/mazzay/auctionator-vanilla/downloads/Auctionator_2.6.3.zip)