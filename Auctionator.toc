## Interface: 11200
## Title: Auctionator |cFFFFFFFF2.6.3|r
## Version: 2.6.3
## Author: Zirco, backported by Mazzay
## X-Website: http://auctionator-addon.com
## X-Localizations: enUS, deDE, ruRU, svSE, frFR, esES
## X-Embeds: Ace2, AceOO-2.0
## Notes: A lightweight addon that makes it easy and fast to buy, sell and manage auctions
## SavedVariablesPerCharacter: AUCTIONATOR_SHOW_ST_PRICE, AUCTIONATOR_ENABLE_ALT, AUCTIONATOR_OPEN_FIRST, AUCTIONATOR_OPEN_BUY, AUCTIONATOR_OPEN_ALL_BAGS, AUCTIONATOR_DEF_DURATION, AUCTIONATOR_SHOW_TIPS, AUCTIONATOR_V_TIPS, AUCTIONATOR_A_TIPS, AUCTIONATOR_D_TIPS, AUCTIONATOR_SHIFT_TIPS, AUCTIONATOR_DE_DETAILS_TIPS, AUCTIONATOR_DEFTAB
## SavedVariables: AUCTIONATOR_SAVEDVARS, AUCTIONATOR_PRICING_HISTORY, AUCTIONATOR_SHOPPING_LISTS, AUCTIONATOR_PRICE_DATABASE, AUCTIONATOR_LAST_SCAN_TIME, AUCTIONATOR_TOONS, AUCTIONATOR_STACKING_PREFS, AUCTIONATOR_SCAN_MINLEVEL

## Ace Libraries
libs\AceLibrary\AceLibrary.lua
libs\AceOO-2.0\AceOO-2.0.lua

Locales\enUS.lua
Locales\deDE.lua
Locales\ruRU.lua
Locales\frFR.lua
Locales\svSE.lua
Locales\esES.lua

zcUtils.lua

Auctionator.lua
AuctionatorAPI.lua
AuctionatorLocalize.lua

AuctionatorPane.lua
AuctionatorShop.lua
AuctionatorBuy.lua
AuctionatorSell.lua
AuctionatorScan.lua
AuctionatorQuery.lua
AuctionatorHints.lua
AuctionatorVendor.lua
AuctionatorConflicts.lua

AuctionatorConfig.lua

Auctionator.xml
AuctionatorConfig.xml

